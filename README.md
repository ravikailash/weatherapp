# Weatherapp

### Description
A simple weather app implementation in Python using OpenWeatherMap APIs

### Requirements
* threading
* json
* requests
* pymongo
* gridfs
* matplotlib
* tkinter
* urllib
* os

### Details
1. API_url: 'http://api.openweathermap.org/data/2.5/forecast'
2. Default-locations: ['Boston', 'San Francisco']


### Database schema
DB_Name: weatherapp
DB_collections = ['fivehrforecast', 'fs.files', 'fs.chunks']
> fivehrforecast : collections for 5 days/ 3 hr records
  fs.files: image(gridfs)
  fs.chunks: image-chunks(gridfs)

```
db.fivehrforecast.Schema > Example
    {"_id" : ObjectId(),
	"date" : "2018-02-19",
	"city" : "Boston",
	"country" : "US",
	"data" : [
		{
			"clouds" : {
				"all" : 64
			},
			"wind" : {
				"deg" : 197.501,
				"speed" : 4.76
			},
			"rain" : {
				"3h" : 0.01
			},
			"main" : {
				"temp_min" : 279.401,
				"pressure" : 1041.73,
				"temp" : 282.5,
				"humidity" : 86,
				"grnd_level" : 1041.73,
				"sea_level" : 1043.27,
				"temp_kf" : 3.1,
				"temp_max" : 282.5
			},
			"snow" : {
			},
			"forcast_time" : 1519074000,
			"weather" : {
				"description" : "light rain",
				"id" : 500,
				"icon" : "10d",
				"main" : "Rain"
			}
		}
	],
	"time" : "21:00:00"
}
```


### Plots and GUI

* **Temperature Plots** (in Kelvin)

![Temperature Boston](assets/Boston-weather.png)

![Temperature San Francisco](assets/Sanfrancisco-weather.png)
