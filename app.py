import sys
try:
    import urllib
    import os
    import threading
    import time
    import json
    import pymongo
    # import gridfs
    import requests
    import matplotlib.pyplot as plt
    import tkinter as tk
    import datetime
    print("All packages are imported successfully...")

except ImportError as e:
    sys.exit("Error importing packages", e)

# function to connect to mongoDB
def establish_database_connection():
    try:
        db_client = pymongo.MongoClient("localhost", 27017)
        print("Successfully created a mongoDB client")
        return db_client
    except:
        raise ConnectionError("Cannot connect to MongoDB")

# connect to the database
db_client = establish_database_connection()
db = db_client["weatherapp"]


# load the configuration file for weather api and locations
with open("config.json") as f:
    details = json.load(f)

locations = details['locations']
api_key = details['weather_owm_api']
weather_api = 'http://api.openweathermap.org/data/2.5/forecast?q='

# alerts
alerts = {}

# temperature for plotting
temperature = {}
temperature_max, temperature_min = {}, {}
temperature_stamp = {}
plt.figure(1)

def initiate_parameters():
    global alerts, temperature, temperature_max, temperature_min, temperature_stamp, locations
    alerts = {}
    temperature = {}
    temperature_max, temperature_min = {}, {}
    temperature_stamp = {}

# updating the locations
def refresh_locations():
    global locations
    with open("config.json") as f:
        details = json.load(f)

    locations = details['locations']
    initiate_parameters()
    print('Locations refreshed')

# function to covert the string to datetime
def str_to_datetime(x):
    t1, t2, t3 = list(map(int, x[0].split("-")))
    t4, t5, _ = list(map(int, x[1].split(":")))
    return datetime.datetime(t1, t2, t3, t4, t5)


def date_formatting(x):
    return '{0}/{1} {2}:{3}'.format(x.month, x.day, x.hour, x.minute)


# lock for the downloading and displaying maps
image_downloading_lock = False
weather_lock = ui_lock = False
# active_lock = threading.Lock()
number = 1

# function to check whether city already exists in database
def city_already_exists(db, _city, _country):
    return db.fivehrforecast.count({"city": _city, "country": _country})


# function to check whether record alrady exists in database
def record_already_exists(db, _city, _country, _date, _time):
    return db.fivehrforecast.find_one({"city": _city, "country": _country, "date": _date, "time": _time}, {"_id":1})

def kelvin_to_farenheit(x):
    return (x * 9 / 5) - 459.67


# function to pre-process the data
def get_details(record):
    r_date, r_time = record['dt_txt'].split(' ')
    r_data = [{"main": record['main'], "weather": record['weather'][0], "wind": record.get('wind', {}),
              "snow": record.get('snow', {}), "rain": record.get('rain', {}), "clouds": record.get('clouds', {}),
              "forcast_time": record['dt']}]

    return r_date, r_time, r_data


# function to update the five hour forecast to database
def update_5days_3hrs(result):
    global db
    global alerts, temperature, temperature_min, temperature_max, temperature_stamp

    _city, _country = result['city']['name'], result['city']['country']

    current_key = _city + '_' + _country
    temperature[current_key] = []
    temperature_max[current_key], temperature_min[current_key] = [], []
    temperature_stamp[current_key] = []
    alerts[current_key] = {}

    if city_already_exists(db, _city, _country):
        for record in result['list']:
            _date, _time, _data = get_details(record)
            temperature_stamp[current_key].append(str_to_datetime((_date, _time)))
            temperature[current_key].append(kelvin_to_farenheit(_data[0]["main"]["temp"]))
            temperature_max[current_key].append(kelvin_to_farenheit(_data[0]["main"].get("temp_max", 0)))
            temperature_min[current_key].append(kelvin_to_farenheit(_data[0]["main"].get("temp_min", 0)))
            if temperature_min[current_key][-1] < 3:
                alerts[current_key]['Date'] = (_date, _time)
                alerts[current_key]['Description'] = _data[0]['weather']['description']
                alerts[current_key]['Minimum temperature'] = _data[0]['main']['temp']

            res = record_already_exists(db, _city, _country, _date, _time)
            if res is None:
                db.fivehrforecast.insert({"city": _city, "country": _country, "date": _date, "time": _time, "data": _data})
            else:
                db.fivehrforecast.find_one_and_update({"_id": res["_id"]}, {"$set": {"data": _data}})
    else:
        for record in result['list']:
            _date, _time, _data = get_details(record)

            temperature_stamp[current_key].append(str_to_datetime((_date, _time)))
            temperature[current_key].append(kelvin_to_farenheit(_data[0]["main"]["temp"]))
            temperature_max[current_key].append(kelvin_to_farenheit(_data[0]["main"].get("temp_max", 0)))
            temperature_min[current_key].append(kelvin_to_farenheit(_data[0]["main"].get("temp_min", 0)))

            db.fivehrforecast.insert({"city": _city, "country": _country, "date": _date, "time": _time, "data": _data})


# thread-1 for downloading 5 days forecast and updating to database
def download_weather_5days_3hrs():
    global locations, weather_api, api_key
    global weather_lock, ui_lock
    weather_lock = ui_lock = True

    for location in locations:
        weather_url = weather_api + location.title() + '&mode=json&appid='+ api_key
        print(weather_url)
        response = requests.get(weather_url)
        result = response.json()
        update_5days_3hrs(result)

    weather_lock = ui_lock = False


# thread-2 to look for weather alerts
def weather_warnings():
    global alerts, weather_lock, ui_lock
    ui_lock = True
    while weather_lock:
        continue

    c, _ = os.get_terminal_size()
    print('=' * c)
    print('=' * c)
    print('Weather_warnings: ')
    print(alerts)
    print('=' * c)
    print('=' * c)
    ui_lock = False

# thread-3 to update the gui
def update_gui():
    global ui_lock
    global temperature, temperature_max, temperature_min
    global temperature_stamp, db, plt
    while ui_lock:
        continue
    print("Updating GUI...")

    index = 1
    plt.close()
    for key in temperature_stamp.keys():
        plt.figure(index)
        t_stamp = [date_formatting(x) for x in temperature_stamp[key]]
        plt.plot(temperature_stamp[key], temperature[key], 'g-', label="Temperature")
        plt.hold(True)
        plt.plot(temperature_stamp[key], temperature_min[key], 'b.', label="Min temperature")
        plt.plot(temperature_stamp[key], temperature_max[key], 'r.', label="Max temperature")
        plt.xticks(temperature_stamp[key], t_stamp, rotation=70)
        plt.legend(frameon=False)
        plt.title(key)
        plt.hold(False)
        index += 1
    plt.show()
    print("Temp plot updated...")


# main driver function
def main():
    while True:
        print(">>> ", "=" * 80)
        forecast = threading.Thread(target=download_weather_5days_3hrs)
        weather_updates = threading.Thread(target=weather_warnings)
        update_display = threading.Thread(target=update_gui)

        refresh_locations()
        initiate_parameters()
        forecast.start()
        weather_updates.start()
        update_display.start()
        time.sleep(120)


# test function
def test():
    '''
    function to test the api-calls, done only 3 times at intervals of 30 seconds
    '''
    global number
    while number < 3:
        print(">>> ", "=" * 80)
        forecast = threading.Thread(target=download_weather_5days_3hrs)
        weather_updates = threading.Thread(target=weather_warnings)
        update_display = threading.Thread(target=update_gui)

        refresh_locations()
        initiate_parameters()
        forecast.start()
        weather_updates.start()
        update_display.start()
        number += 1
        time.sleep(10)


if __name__ == "__main__":
    test()



# # examples
# weather_5days_3hrs = 'http://api.openweathermap.org/data/2.5/forecast?q=Boston,us&mode=json&appid=27847a2f36032495c21e92f539c7d2ae'
# weather_map = 'http://openweathermap.org/weathermap?basemap=map&cities=false&layer=windspeed&lat=44.9026&lon=-72.4438&zoom=6'


# def f3():
#     global number
#     global image_downloading_lock, active_lock
#
#     image_downloading_lock = True
#     # active_lock.acquire()
#     print('This is function 3')
#     f = open(str(number) + '.jpg', 'wb')
#     r = requests.get('http://www.gunnerkrigg.com//comics/00000001.jpg')
#     if r.ok:
#         f.write(r.content)
#     f.close()
#     image_downloading_lock = False
#     # active_lock.release()

# def f4():
#     global image_downloading_lock, active_lock
#     x = 1
#     while image_downloading_lock:
#         print("Waiting for the thread release ", x)
#         x += 1
#         continue
#
#     print('='*100)
#     img = Image.open(str(number) + '.jpg')
#     img.show()


# [{"main" : {"sea_level":1043.27,"temp":110,"temp_max":274.295,"temp_kf":-0.22,"temp_min":274.08,"grnd_level":1041.7,"humidity":100,"pressure":1041.7},"clouds":{"all":0},"wind":{"deg":299.001,"speed":1.8},"rain":{},"weather":{"main":"Clear","id":800,"icon":"01n","description":"clear sky"},"snow":{},"forcast_time":1518998400}]
